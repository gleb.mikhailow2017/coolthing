﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BadPlat : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D coll)
    {
        if(coll.gameObject.tag == "Player")
        {
            GetComponent<Animation>().Play();
            if (GetComponent<Animation>().isPlaying == false) Destroy(gameObject);
            GetComponent<PolygonCollider2D>().enabled = false;
        }
    }
}
