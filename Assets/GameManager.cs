﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class GameManager : MonoBehaviour
{
    string path = @"C:\Games\SavedGame.txt";



    int _karma = 0;
    int tmp;
    int sbi;

    public Candles can;
    public void Start()
    {
        DontDestroyOnLoad(gameObject);
        try
        {
            using (StreamReader sr = new StreamReader(path))
            {
                if (sr.ReadLine() != null)
                {
                    string[] a = sr.ReadLine().Split(' ');
                    tmp = int.Parse(a[0]);
                    _karma = int.Parse(a[1]);
                    can.CheckPoint = int.Parse(a[2]);
                    can.candls = int.Parse(a[3]);
                }
            }
        }
        catch (Exception e)
        {
            print(e);
        }

    }


    public void Continue()
    {
        SceneManager.LoadScene(tmp);
        can.ui.enabled = true;
        for (int i = 0; i < FindObjectsOfType<CheckPoint>().Length; i++)
        {
            CheckPoint cp = FindObjectsOfType<CheckPoint>()[i];
            if(can.CheckPoint > cp.id)
                Destroy(cp.gameObject);
            if (can.CheckPoint == cp.id)
                can.LoadPl(cp.gameObject.transform.position);
        }
    }


    public void NewGame()
    {
        _karma = 0;
        can.CheckPoint = 0;
        can.candls = 2;
        tmp = 1;
        can.ui.enabled = true;
        can.inRoom = false;
        SceneManager.LoadScene(tmp);
    }
    

    public void Memory(int memnum)
    {
        tmp = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(memnum);
    }
    public void MemOut(bool karma)
    {
        if (karma)
        {
            _karma++;
        }
        else
        {
            _karma--;
        }
        SceneManager.LoadScene(tmp);
    }
    public void ExitLvl()
    {
        sbi = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(sbi + 1);
    }

    public void ExitGame()
    {
        save();
        Application.Quit();
        
    }

    public void Ending()
    {
        if (_karma == 2)
        {
            SceneManager.LoadScene(4);
        }
        if (_karma == -2)
        {
            SceneManager.LoadScene(7);
        }
        if (_karma > -2 || _karma < 2)
        {
            SceneManager.LoadScene(8);
        }
    }

    public void End()
    {

    }

    public void save()
    {
        try
        {
            using (StreamWriter sw = new StreamWriter(path, false, System.Text.Encoding.Default))
            {
                sw.WriteLine(SceneManager.GetActiveScene().buildIndex +" "+ _karma + " " +can.CheckPoint +" "+can.candls);
            }
        }
        catch (Exception e)
        {
            print(e.Message);
        }
    }
}
