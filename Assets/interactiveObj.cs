﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class interactiveObj : MonoBehaviour
{
    public bool act = false;
    public GameObject plat;
    private void Start()
    {
        plat.SetActive(false);
    }
    void Update()
    {
        if (act)
        {
            plat.SetActive(true);
        }
    }
}
