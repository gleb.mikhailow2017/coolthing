﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Interact : MonoBehaviour
{
    [SerializeField] Candles candlcontr;

    [SerializeField] int keys;
    [SerializeField] int items;
    public GameObject tmpIt;
    public GameObject check;
    public GameObject tmp;
    public GameObject tmpDr;
    public GameObject tmpMm;
    public GameManager GM;
    [SerializeField] int i;
    void Start()
    {
        candlcontr = FindObjectOfType<Candles>();
        candlcontr.player = gameObject;
        GM = GameObject.FindObjectOfType<GameManager>();
    }
    void Update()
    {
        if (i == 1 && Input.GetKeyDown(KeyCode.E))
        {
            items++;
            Destroy(tmp);
        }

        if (i == 2 && Input.GetKeyDown(KeyCode.E) && tmpIt.GetComponent<interactiveObj>() && items > 0)
        {
            items--;
            tmpIt.GetComponent<interactiveObj>().act = true;
        }
        if (i == 2 && Input.GetKeyDown(KeyCode.E) && tmp.GetComponent<Wall>() && keys > 0)
        {
            keys--;
            tmp.GetComponent<Wall>().act = true;
        }

        if (i == 3 && Input.GetKeyDown(KeyCode.E))
        {
            gameObject.transform.position = tmpDr.GetComponent<Room>().room.position;
            candlcontr.inRoom = tmpDr.GetComponent<Room>().IsRoom;
        }

        if (i == 4 && Input.GetKeyDown(KeyCode.E))
        {
            GM.Memory(tmpMm.GetComponent<MemInd>().indexMem);
        }

        if (i == 5 && Input.GetKeyDown(KeyCode.E))
        {
            keys++;
            Destroy(tmp);
        }

        if (i == 6 && Input.GetKeyDown(KeyCode.E))
        {
            GM.Ending();
        }

        if (i == 7 && Input.GetKeyDown(KeyCode.E))
        {
            candlcontr.Check(check.GetComponent<CheckPoint>().id, check);

        }
    }
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Item") { i = 1; tmp = col.gameObject; }
        if (col.gameObject.tag == "PlatPl") { i = 2; tmpIt = col.gameObject; }
        if (col.gameObject.tag == "door") { i = 3; tmpDr = col.gameObject; }
        if (col.gameObject.tag == "Spawn") Respawn();
        if (col.gameObject.tag == "Memory") { i = 4; tmpMm = col.gameObject; }
        if (col.gameObject.tag == "key") { i = 5; tmp = col.gameObject; }
        if (col.gameObject.tag == "wall") { i = 2; tmpIt = col.gameObject; }
        if (col.gameObject.tag == "End") i = 6;
        if (col.gameObject.tag == "LvlD") GM.ExitLvl(); ;
        if (col.gameObject.tag == "Dead") GM.ExitGame();

        if (col != null && col.gameObject.tag == "CheckPoint") { i = 7; check = col.gameObject; }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.gameObject.tag != "Untagged")
        i = 0;
    }

    void Respawn()
    {
        gameObject.transform.position = GameObject.FindGameObjectWithTag("Respawn").transform.position;
    }
    
}
