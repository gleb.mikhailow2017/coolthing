﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ends : MonoBehaviour
{
    GameManager Gm;
    public bool End;
    private void Awake()
    {
        Gm = GameObject.FindObjectOfType<GameManager>();
    }
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        Gm.MemOut(End);
    }
}
