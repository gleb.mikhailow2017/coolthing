﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Trash : MonoBehaviour
{
    public GameObject Room;
    public bool coal_powder, gunpowder, sulfur, bomb = false;

    public Image gunpowderSpr, sulfurSpr, bombSpr, coalSpr;
    public Canvas Inv;

    private void Update()
    {
        if (Inv.transform.GetChild(0).childCount == 3)
        {
            Destroy(Inv.transform.GetChild(0).GetChild(0).gameObject);
            Destroy(Inv.transform.GetChild(0).GetChild(0).gameObject);
            Destroy(Inv.transform.GetChild(0).GetChild(0).gameObject);
            GameObject.Instantiate(bombSpr, Inv.transform.GetChild(0));
            bomb = true;
        }
    }
    private void Start()
    {
        Room.SetActive(false);
        Inv.enabled = false;
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "gpwd")
        {
            GameObject.Instantiate(gunpowderSpr,Inv.transform.GetChild(0).transform);
            Destroy(col.gameObject);
        } 
        if(col.gameObject.tag == "cpwd")
        {
                Destroy(col.gameObject);
                GameObject.Instantiate(coalSpr, Inv.transform.GetChild(0).transform);
        }
        if(col.gameObject.tag == "slf")
        {
                Destroy(col.gameObject);
                GameObject.Instantiate(sulfurSpr, Inv.transform.GetChild(0).transform);
        }
        if (col.gameObject.tag == "Trsh" && bomb)
        {
            col.GetComponent<TrashPlanks>().Boom();
                Destroy(Inv.transform.GetChild(0).gameObject);
        }
    }
}
