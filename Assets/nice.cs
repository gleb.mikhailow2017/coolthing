﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class nice : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision && collision.tag == "Dead")
        {
            collision.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePosition;
        }
        if (collision && collision.tag == "Finish")
        {
            Application.Quit();
        }
    }
}