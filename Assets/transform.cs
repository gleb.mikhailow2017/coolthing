﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class transform : MonoBehaviour
{
    [SerializeField] bool start = false;
    private void Update()
    {
        if(start)
        GetComponent<Rigidbody2D>().velocity = new Vector2(6f,0);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject != null && collision.gameObject.tag == "Player") start = true;
    }
}
