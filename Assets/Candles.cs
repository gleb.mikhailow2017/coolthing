﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Candles : MonoBehaviour
{
    public Canvas ui;

    public int CheckPoint;

    public Text _lifes;

    public GameObject player;

    public int lifes;
    public float tmpMH, tmpM, mind, mindH;

    public bool inRoom;

    public int candls;
    public float candlTime;
    public float tmp;

    public GameObject _candle;
    public Image Mind;

    public GameManager GM;
    public Image CandlePar, Light;
    public Sprite light, dark;

    [SerializeField] bool IsLight;

    public bool pickCandle;


    private void Start()
    {
        tmpMH = mindH;
        tmpM = mind;
        candls = 3;
        tmp = candlTime;
        DontDestroyOnLoad(gameObject);
        DontDestroyOnLoad(ui);
        IsLight = true;

        ui.enabled = false;

    }

    public void Check(int id, GameObject candle)
    {
        CheckPoint = id;
        if (candls < 3)
        {
            candls++;
            GameObject.Instantiate(_candle, CandlePar.transform);
        }
        else
        {
            tmp = candlTime;
        }
        candle.GetComponent<CheckPoint>().candle--;
    }

    public void LoadPl(Vector2 check)
    {
        player.transform.position = check;
    }

    private void Update()
    {
        _lifes.text = lifes.ToString();

        if (!ui.enabled)
        {
            inRoom = true;
        }

        if (IsLight && !inRoom)
        {
            if (tmp > 0)
            {
                tmp -= Time.deltaTime;
                CandlePar.transform.GetChild(0).GetComponent<Image>().fillAmount = (tmp / candlTime);
            }
            else
            {
                candls--;
                tmp = candlTime;
                
                Destroy(CandlePar.transform.GetChild(CandlePar.transform.childCount-1).gameObject);
            }
        }

        if (candls == 0) IsLight = false;

        if (IsLight)
            Light.sprite = light;
        else
            Light.sprite = dark;

        if (Input.GetKeyDown(KeyCode.C))
            IsLight = !IsLight;

        if (inRoom) IsLight = true;




        if (!inRoom && !IsLight)
        {
            if (lifes > 0)
            {
                if (tmpM < 0)
                {
                    lifes--;
                    tmpM = mind;
                }
                else
                {
                    tmpM -= Time.deltaTime;
                    Mind.fillAmount = tmpM / mind;
                }
            }
        }
        else
        {
            if (lifes < 3)
            {
                if (tmpMH > mindH)
                {
                    lifes++;
                    tmpMH = 0;
                }
                else
                {
                    tmpMH += Time.deltaTime;
                    Mind.fillAmount = tmpMH / mindH;
                }
            }
        }
    }
}
