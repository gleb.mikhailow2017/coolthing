﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndEit : MonoBehaviour
{
    GameManager GM;
    private void Start()
    {
        GM = FindObjectOfType<GameManager>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player") GM.ExitGame();
    }
}
